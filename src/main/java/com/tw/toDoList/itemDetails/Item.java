package com.tw.toDoList.itemDetails;

import javax.persistence.*;

@Table
@Entity
public class Item {
    @Id
    @SequenceGenerator(
            name = "item_sequence",
            sequenceName = "item_sequence",
            allocationSize = 1
    )    //to initialize the sequence
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "item_sequence"
    )     //to generate the sequence

    private long item_id;
    private String item_name;
    private int  item_status;


    public Item(String item_name, int item_status) {
        this.item_name = item_name;
        this.item_status = item_status;
    }

    public Item(){}     //default constructor

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public int getItem_status() {
        return item_status;
    }

    public void setItem_status(int item_status) {
        this.item_status = item_status;
    }
}

