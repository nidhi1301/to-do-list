package com.tw.toDoList.itemDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/to-do-list/items")
public class ItemController {
    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping
    public List<Item> itemList() { return itemService.viewList(); }

    @PostMapping
    public Item addItem(@RequestBody Item item){
        itemService.add(item);
        return item;
    }

    @DeleteMapping("/{item_id}")
    public void removeItem(@PathVariable("item_id") Long item_id){
        itemService.remove(item_id);
    }

    @PutMapping("/{item_id}")
    public void updateItem(
            @PathVariable("item_id") Long item_id,
            @RequestParam(required = false) String item_name,
            @RequestParam(required = false) Integer item_status) {
        itemService.update(item_id, item_name, item_status);
    }

}

