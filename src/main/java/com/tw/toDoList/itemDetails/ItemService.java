package com.tw.toDoList.itemDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ItemService {
    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> viewList(){
        return itemRepository.findAll();
    }

    public void add(Item item) {
        itemRepository.save(item);
    }

    public void remove(Long item_id) {
        if (!itemRepository.existsById(item_id)) {
            throw new IllegalArgumentException("Item with id " + item_id + " does not exist.");
        }
        itemRepository.deleteById(item_id);
    }

    @Transactional
    public void update(Long item_id, String item_name, Integer item_status) {
        Item item = itemRepository.findById(item_id).
                orElseThrow(() -> new IllegalArgumentException(
                        "Item with id " + item_id + " does not exist."));

//        if (item_name != null
//                && item_name.length() > 0
//                && !Objects.equals(item.getItem_name(), item_name)) {
            item.setItem_name(item_name);
            item.setItem_status(item_status);
     //   }

//        if (item_status == 0
//                && !Objects.equals(item.getItem_status(), item_status)) {
//            item.setItem_status(item_status);
//        }
//
//        if (item_status == 1
//                && !Objects.equals(item.getItem_status(), item_status)) {
//            item.setItem_status(item_status);
//        }

    }

}

