package com.tw.toDoList.itemDetails;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ItemConfiguration {
    @Bean
    CommandLineRunner commandLineRunner(ItemRepository itemRepository) {
        return args -> {
            Item item_one = new Item("item_one", 0);
            Item item_two = new Item("item_two", 1);

           itemRepository.saveAll(List.of(item_one,item_two));
        };
    }
}
