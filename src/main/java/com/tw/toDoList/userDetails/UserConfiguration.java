package com.tw.toDoList.userDetails;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class UserConfiguration {
    @Bean
    CommandLineRunner userCommandLineRunner(UserRepository userRepository) {
        return args -> {
            User user_one = new User("user_one", "one@gmail.com", "abc");
            User user_two = new User("user_two", "two@gmail.com", "xyz");

            userRepository.saveAll(List.of(user_one,user_two));
        };
    }
}