package com.tw.toDoList.userDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> viewList(){
        return userRepository.findAll();
    }

    public void add(User user) {
        userRepository.save(user);
    }

    public void remove(Long user_id) {
        if (!userRepository.existsById(user_id)) {
            throw new IllegalArgumentException("User does not exist.");
        }
        userRepository.deleteById(user_id);
    }
}
