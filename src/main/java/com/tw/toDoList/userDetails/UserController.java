package com.tw.toDoList.userDetails;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/to-do-list/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> userList() { return userService.viewList(); }

    @PostMapping
    public User addUser(@RequestBody User user){
        userService.add(user);
        return user;
    }

    @DeleteMapping("/{user_id}")
    public void removeUser(@PathVariable("user_id") Long user_id){
        userService.remove(user_id);
    }
}
