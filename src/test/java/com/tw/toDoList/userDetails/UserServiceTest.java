package com.tw.toDoList.userDetails;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService service;

    User user;
    List<User> userList;
    @BeforeEach
    void setup(){
        user = new User("user", "user@gmail.com", "mno");
        user.setUser_id(1);
        userList = List.of(user);
    }

    @Test
    public void shouldReturnAddedItemWhenGivenItemToAdd() {
        when(userRepository.save(any())).thenReturn(user);
        service.add(user);
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void shouldReturnListOfAllItemsWhenGivenGetAllItems() {
     userRepository.save(user);
        when(userRepository.findAll()).thenReturn(userList);
        List<User> newUserList = service.viewList();
        assertEquals(newUserList, userList);
        verify(userRepository, times(1)).save(user);
        verify(userRepository, times(1)).findAll();
    }


    @Test
    public void shouldThrowExceptionWhenItemIdIsNotExistAndTryToDelete() {
        when(userRepository.existsById(anyLong())).thenReturn(false);

        assertThrows(IllegalArgumentException.class, () -> service.remove(anyLong()));

        verify(userRepository).existsById(anyLong());
        verify(userRepository, times(0)).deleteById(anyLong());
    }

    @Test
    public void shouldDeleteTheItemWhenGivenIdToDeleteExist() {
        when(userRepository.existsById(user.getUser_id())).thenReturn(true);
        service.remove(user.getUser_id());
        verify(userRepository).deleteById(user.getUser_id());
    }


}
