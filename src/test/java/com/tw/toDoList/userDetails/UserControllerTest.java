package com.tw.toDoList.userDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tw.toDoList.itemDetails.ItemController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    UserService userService;

    User user;
    List<User> userList;
    @BeforeEach
    void setup(){
        user = new User("new_user", "user@gmail.com","ghi");
        userList = List.of(user);
    }

    @Test
    void shouldViewListWhenUserSendsAGetRequest() throws Exception {
        Mockito.when(userService.viewList()).thenReturn(userList);

        mockMvc.perform(get("/to-do-list/users")).
                andExpect(status().isOk());

        Mockito.verify(userService).viewList();
    }


    @Test
    void shouldAddUserToListWhenUserSendsAPostRequest() throws Exception {
        doNothing().when(userService).add(user);

        mockMvc.perform(post("/to-do-list/users").
                        contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsString(user)).
                        accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk());
    }

    @Test
    void shouldDeleteUserFromListWhenUserSendsADeleteRequest() throws Exception {
        doNothing().when(userService).remove(anyLong());

        mockMvc.perform(delete("/to-do-list/users/{user_id}", user.getUser_id())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());

        Mockito.verify(userService,times(1)).remove(user.getUser_id());
    }

}
