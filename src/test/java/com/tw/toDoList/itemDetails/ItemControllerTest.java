package com.tw.toDoList.itemDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ItemService itemService;

    Item item;
    List<Item> itemList;
    @BeforeEach
    void setup(){
        item = new Item("item", 0);
        itemList = List.of(item);
    }

    @Test
    void shouldViewListWhenUserSendsAGetRequest() throws Exception {
        Mockito.when(itemService.viewList()).thenReturn(itemList);

        mockMvc.perform(get("/to-do-list/items")).
                andExpect(status().isOk());

        Mockito.verify(itemService).viewList();
    }


    @Test
    void shouldAddItemToListWhenUserSendsAPostRequest() throws Exception {
        doNothing().when(itemService).add(item);

        mockMvc.perform(post("/to-do-list/items").
                        contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsString(item)).
                        accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk());
    }

    @Test
    void shouldDeleteItemFromListWhenUserSendsADeleteRequest() throws Exception {
        doNothing().when(itemService).remove(anyLong());

        mockMvc.perform(delete("/to-do-list/items/{item_id}", item.getItem_id())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(item)))
                .andExpect(status().isOk());

        Mockito.verify(itemService,times(1)).remove(item.getItem_id());
    }

    @Test
    void shouldUpdateItemWhenUserSendsAPutRequest() throws Exception{
        doNothing().when(itemService).update(item.getItem_id(), null, null);

        mockMvc.perform(put("/to-do-list/items/{item_id}", item.getItem_id())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(item)))
                .andExpect(status().isOk());

        verify(itemService)
                .update(item.getItem_id(), null,null);
    }

}