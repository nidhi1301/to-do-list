package com.tw.toDoList.itemDetails;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ItemServiceTest {
    @Mock
    ItemRepository itemRepository;

    @InjectMocks
    ItemService service;

    Item item;
    List<Item> itemList;
    @BeforeEach
    void setup(){
        item = new Item("item", 1);
        item.setItem_id(1);
        itemList = List.of(item);
    }

    @Test
    public void shouldReturnAddedItemWhenGivenItemToAdd() {
        when(itemRepository.save(any())).thenReturn(item);
        service.add(item);
        verify(itemRepository, times(1)).save(any());
    }

    @Test
    public void shouldReturnListOfAllItemsWhenGivenGetAllItems() {
        itemRepository.save(item);
        when(itemRepository.findAll()).thenReturn(itemList);
        List<Item> newItemList = service.viewList();
        assertEquals(newItemList, itemList);
        verify(itemRepository, times(1)).save(item);
        verify(itemRepository, times(1)).findAll();
    }


    @Test
    public void shouldThrowExceptionWhenItemIdIsNotExistAndTryToDelete() {
        when(itemRepository.existsById(anyLong())).thenReturn(false);

        assertThrows(IllegalArgumentException.class, () -> service.remove(anyLong()));

        verify(itemRepository).existsById(anyLong());
        verify(itemRepository, times(0)).deleteById(anyLong());
    }

    @Test
    public void shouldDeleteTheItemWhenGivenIdToDeleteExist() {
        when(itemRepository.existsById(item.getItem_id())).thenReturn(true);
        service.remove(item.getItem_id());
        verify(itemRepository).deleteById(item.getItem_id());
    }

    @Test
    public void shouldUpdateItemToZeroStatusWhenGivenIdToUpdateExist() {
        item = new Item("item", 1);
        item.setItem_id(1);

        given(itemRepository.findById(item.getItem_id())).willReturn(Optional.of(item));
        service.update(item.getItem_id(), "newItem", 0);
        verify(itemRepository).findById(item.getItem_id());
    }

    @Test
    public void shouldUpdateItemToOneStatusWhenGivenIdToUpdateExist() {
        item = new Item("item", 0);
        item.setItem_id(1);

        given(itemRepository.findById(item.getItem_id())).willReturn(Optional.of(item));
        service.update(item.getItem_id(), "newItem", 1);
        verify(itemRepository).findById(item.getItem_id());
    }

}